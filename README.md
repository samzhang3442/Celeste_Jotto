# Celeste_Jotto

This is Stony Brook University CSE308 team project in the spring of 2019 which is created by Jing Zhang, Yifan Wang, Yingnan Li and Weihao Li.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [How to play](#how-to-play)
* [Contact info](#contact-info)

## General info

**Jotto** (or Giotto) is a logic-oriented word game played with two players, a writing implement, and a piece of paper with the alphabet on it. Each player writes a secret word and attempts to guess the other player's word.

**Celeste_Jotto** is an online man-machine interaction Jotto game webapp. User can play Jotto with our game AI.

Every user is required to register a unique user account with legal username and password. Also, Celeste_Jotto will record finished game automatically, so users can look back their game details at any time.
	
## Technologies
Project is created with:
* Java Spring Boot - the back end of this webapp, handling webpage request and return data to front end
* JSP - the front end of this webapp, the User interface and send request to back end
* JQuery - write for web page function
* MySQL - Celeste_Jotto's database, stored the user information, such as username, password and each game details
* Microsoft Azure - deploy whole project into Azure to make a real online webapp
* Java - the game program, to determine the winner and return feedback of each guess
	

## Setup
To set up and deploy this project, you need:

* Create/Have your Azure account 
* Set up the Azure MySQL database
* change the setting up information under \src\main\resources\application.properties
* Deploy this project into your Azure space


## How to play

1. Create your own account
2. After login successfully, choose new game button
3. Choose your own 5-letters secret word, then start the game
4. In the game page, you can select to guess by your own or let computer choose a random word for you
5. After your send the guess word, you will get the feedback of this turn, using feedback to make a better guess until winner is produced

## Contact info

samzhang3442@gmail.com
