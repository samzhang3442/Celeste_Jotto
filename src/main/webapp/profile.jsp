<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--<!DOCTYPE html>--%>
<!-- saved from url=(0042)https://modular-admin-html.modularcode.io/ -->
<html class="no-js gr__modular-admin-html_modularcode_io" lang="en" id="Main"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--<base href="/">--><base href=".">
    
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Celeste Jotto </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://modular-admin-html.modularcode.io/favicon.ico">
    <link rel="stylesheet" href="./ModularAdmin _ HTML version_files/css">
    <link rel="stylesheet" href="./ModularAdmin _ HTML version_files/bundle.css"> <style type="text/css">/* Chart.js */


  @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style><style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0);background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;box-sizing: content-box;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>
  <body data-gr-c-s-loaded="true">
    <!--
  You may use the following felper classes on the .App element

  -dafault                    Default layout options

  -header-fixed               Fixed header on all viewports
  -header-fixed-mobile        Fixed header on all viewports
  -sidebar-compact-tablet     Compact sidebar on tablet viewports
  -sidebar-open-tablet        Open sidebar on tabler viewports
  -sidebar-compact-desktop    Compact sidebar on desktop viewports
  -customize-open             Open customize menu panel
-->
    <div id="App" class="
    App
  ">
      <div class="HeaderContainer">
        <header class="AppHeader">
          <div class="HeaderBlock">
			<%--<form action="/chooseWord">--%>
              <%--<button onclick="location.href = '/chooseWord';" class="Button -primary ControlButton" id="HeaderSidebarToggleButton">
                Play Jotto!!
              </button>--%>
			<%--</form>--%>
              <button class="Button -primary ControlButton" id="HeaderSidebarToggleButton" style="color: #509C9C;">
                  Welcome, ${founduser.username}
              </button>
          </div>
          <div class="HeaderBlock">
            <nav class="ProjectNav">
              <!--<div class="NavBarTitle">%{username}</div>-->
            </nav>
          </div>
          <div class="HeaderBlock">
            <nav class="ProfileNav">
              <div class="Dropdown NavItem Profile">
                <button onclick="location.href = '/logout';" class="Button -primary ControlButton" id="HeaderSearchToggleButto"
                    style="color: #509C9C;">
				 Logout
				</button>
              </div>
            </nav>
          </div>
        </header>
      </div>
      <div class="SidebarOverlay" id="SidebarOverlay"></div>
      <div class="ContentContainer Dashboard">
        <div class="Content">
          <article class="Page Dashboard">
            <div class="PageContainer">
              <div class="Intro">
                  <%--<h1 class="Title"> Welcome, ${founduser.username} </h1>--%>
                <br>
                  <%--<h1 class="Title"> Your Profile </h1>--%>
                  <button onclick="location.href = '/chooseWord';" class="Button -primary"
                          id="HeaderSidebarToggleButton">
                      <h1 style="color: #509C9C;">Play Jotto!!</h1>
                  </button>
              </div>
              <div class="row -row-compact-sm -row-compact-md -row-compact-lg">        
			  <div class="row -row-compact-sm -row-compact-md -row-compact-lg">
                <!--<div class="col-xl-8 -sameheight">-->
                  <div class="Card Items" id="DashboardItems">
                    <div class="CardHeader  -bordered -sm">
                      <div class="HeaderBlock">
                          <h4 class="Title"> Recent Games (Click the number for details)</h4>
                      </div>
                    </div>
                    <div class="ResponsiveList -striped">
                      <div class="ListHeader -hidden-xs -hidden-sm">
                        <div class="ItemCol ColSales -rel-size-2 -size-xs-12 -rel-size-xs-12"> GameID </div>
                          <div class="ItemCol ColStats -rel-size-2 -size-xs-12 ColStats"> Result</div>
                          <div class="ItemCol ColDate -rel-size-2 -size-xs-12 ColDate"> Date</div>
                      </div>
                      <div class="ListContent">


                      <c:choose>
                      <c:when test="${not empty games}">
						<c:forEach items = "${games}" var = "game">

						<div class="ListItem">
                          <div class="Divider -visible-xs -visible-sm -order-sm--1"> </div>
                          <div class="ItemCol ColSales -rel-size-2">
                            <div class="ItemLabel -visible-xs"> Game ID </div>
                            <div class="ItemValue">

                                <form id="gameResultOfThisID" method="post" action="/GameHistory" style="padding: 0; margin: 0;">
                                <input type="hidden" name="gid" value="${game.gameid}">
                                    <div class="ItemValue" style="color: #509C9C;cursor: pointer; text-decoration: underline;" onclick="document.getElementById('gameResultOfThisID').submit();"> ${game.gameid} </div>
                              </form>

                            </div>
                          </div>

                          <div class="Divider -visible-xs"> </div>
                          <div class="ItemCol ColStats -rel-size-2">
							<div class="ItemLabel -visible-xs"> Status </div>
                            <div class="ItemValue"> ${game.game_Result} </div>
						  </div>


                          <div class="Divider -visible-xs"> </div>
                          <div class="ItemCol ColDate -rel-size-2">
                            <div class="ItemLabel -visible-xs"> Date </div>
                              <div class="ItemValue">${game.createdAt.toString().substring(0,19)}</div>
                            <%--<div class="ItemValue"> ${game.createdAt.month}-${game.createdAt.day}-${game.createdAt.year}--%>
                                    <%--${game.createdAt.hours}:${game.createdAt.minutes}:${game.createdAt.seconds} ${game.createdAt.toString()}</div>--%>
                          </div>
                        </div>
                        </c:forEach>
                      </c:when>
                      </c:choose>
					   
					   
					   
					   
                      </div>
                      <div class="ListFooter"> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           </article>
        </div>
      </div>
        <%--<div class="FooterContainer">--%>
        <%--<footer class="Footer AppFooter">--%>
        <%--<p>Posted by: Weihao Li, Yifan Wang, Jing Zhang, Yingnan Li</p>--%>
        <%--<p>Contact information:<a href="yingnan.li@stonybrook.edu"> yingnan.li@stonybrook.edu</a>.</p>--%>
        <%--</footer>--%>
        <%--</div>--%>
        <%--<div class="CustomizeContainer">--%>
        <%--<div class="AppCustomize">--%>
        <%--<header class="CustomizeHeader">--%>
        <%--<h4 class="Title"> Customize Theme </h4>--%>
        <%--<a href="https://modular-admin-html.modularcode.io/" id="CustomizeDismissButton" class="Button -dismiss CustomizeDismissButton">--%>
        <%--<i class="fa fa-times Icon"></i>--%>
        <%--</a>--%>
        <%--</header>--%>
        <%--<div class="CustomizeContent"> </div>--%>
        <%--</div>--%>
        <%--</div>--%>
        </div>
    <script src="./ModularAdmin _ HTML version_files/bundle.js"></script>
  
</body></html>