<mvc:annotation-driven ignoreDefaultModelOnRedirect="true"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- saved from url=(0042)https://modular-admin-html.modularcode.io/ -->
<html class="no-js gr__modular-admin-html_modularcode_io" lang="en" id="Main">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--<base href="/">-->
    <base href=".">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Celeste Jotto </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://modular-admin-html.modularcode.io/favicon.ico">
    <link rel="stylesheet" href="./ModularAdmin _ HTML version_files/css">
    <link rel="stylesheet" href="./ModularAdmin _ HTML version_files/bundle.css">
    <style type="text/css">/* Chart.js */
    @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}
    </style>
    <style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) ;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;box-sizing: content-box;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style>
</head>
<body data-gr-c-s-loaded="true">
<!--
   You may use the following felper classes on the .App element

   -dafault                    Default layout options

   -header-fixed               Fixed header on all viewports
   -header-fixed-mobile        Fixed header on all viewports
   -sidebar-compact-tablet     Compact sidebar on tablet viewports
   -sidebar-open-tablet        Open sidebar on tabler viewports
   -sidebar-compact-desktop    Compact sidebar on desktop viewports
   -customize-open             Open customize menu panel
   -->
<div id="App" class="App">
    <div class="HeaderContainer">
        <header class="AppHeader">
            <div class="HeaderBlock">

                <form method="get" action="login" style="margin: 0; padding: 0;">
                    <button class="Button -primary ControlButton" style="color: #509C9C;">Celeste Jotto</button>
                </form>

            </div>
            <div class="HeaderBlock">
                <nav class="ProfileNav">
                    <div class="Dropdown NavItem Profile">
                        <form method="get" action="login" style="margin: 0; padding: 0;">
                            <button class="Button -primary ControlButton" id="AboutUsButton" style="color: #509C9C;">Profile</button>
                        </form>
                        <form method="get" action="index" style="margin: 0; padding: 0;">
                            <button class="Button -primary ControlButton" id="HeaderSearchToggleButto" style="color: #509C9C;">
                                Logout
                            </button>
                        </form>
                    </div>
                </nav>
            </div>
        </header>
    </div>
    <div class="SidebarOverlay" id="SidebarOverlay"></div>
    <div class="ContentContainer Dashboard">
        <div class="Content">
            <article class="Page Dashboard">
                <div class="PageContainer" style="padding-top:8%;">
                    <div class="MainTitle">
                        <h1 class="Title" style="margin: 0; font-weight: 300; font-size: 1.8em; text-align: center;">
                            Choose Your Secret Word
                        </h1>
                        <div class="Title"
                             style="margin: 8px 2px 0 0; font-weight: 300; font-size: 0.8em; text-align: center;">
                            * Your secret word should be a valid word with 5 unique letters
                        </div>
                    </div>

                    <%--<div class="row -row-compact-sm -row-compact-md -row-compact-lg">--%>
                    <%--<!--<div class="col-xl-8 -sameheight">-->--%>
                    <%--<!-- bootstrap /4 input-->--%>
                    <%--<form method="POST" action="/chooseWord">--%>
                    <%--<div class="input-group input-group-lg" style="padding-left: 30%; padding-right: 30%; margin-top: 10px; margin-bottom: 10px; min-width: 5em;">--%>
                    <%--<input type="text" class="form-control" name="userWord" required value="${currentGame.getActualWordUser()}" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" required style="text-align: center;">--%>
                    <%--</div>--%>
                    <%--<!-- Error -->--%>
                    <%--<div class="input-group input-group-lg" style="padding-left: 30%; padding-right: 30%; margin: 0; text-align: center;">--%>
                    <%--<c:if test="${not empty error}">--%>
                    <%--<div class="Title" style="margin: 0; font-weight: 300; font-size: 1.8em; text-align: center; color: #d32f2f; font-size: 1.1em;">${error}</div>--%>
                    <%--</c:if>--%>
                    <%--</div>--%>
                    <%--<div class="input-group input-group-lg" style="padding-left: 46%; padding-right: 46%; margin-top: 10px; margin-bottom: 10px;">--%>
                    <%--<input type="submit" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" value="submit">--%>
                    <%--</div>--%>
                    <%--</form>--%>
                    <%--<form action="/chooseWord" method="POST">--%>
                    <%--<div class="input-group input-group-lg" style="padding-left: 46%; padding-right: 46%; margin-top: 30px; margin-bottom: 10px;">--%>
                    <%--<input hidden="hidden" name="getRandom" type="checkbox" checked="checked">--%>
                    <%--<input type="submit" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" value="random">--%>
                    <%--</div>--%>
                    <%--</form>--%>
                    <%--</div>--%>

                    <div class="row -row-compact-sm -row-compact-md -row-compact-lg" style="width: 40%; margin: 0 30%;">
                        <table style="margin: 0 30%; width:40%; padding: 0;">
                            <col width="80%"/>
                            <col width="20%"/>
                            <tr>
                                <td>
                                    <form method="POST" action="/chooseWord">
                                        <div class="input-group input-group-lg" style="text-align: center">
                                            <input type="text" class="form-control" name="userWord" required
                                                   value="${currentGame.getActualWordUser()}"
                                                   aria-label="Sizing example input"
                                                   aria-describedby="inputGroup-sizing-lg"
                                                   required style="text-align: center; width: 60%; margin-right: 1%;">

                                            <input type="submit" class="form-control" aria-label="Sizing example input"
                                                   aria-describedby="inputGroup-sizing-lg" value="Submit"
                                                   style="text-align: center; width: 20%;">
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <form action="/chooseWord" method="POST">
                                        <div class="input-group input-group-lg" style="">
                                            <input hidden="hidden" name="getRandom" type="checkbox" checked="checked">
                                            <input type="submit" class="form-control" aria-label="Sizing example input"
                                                   aria-describedby="inputGroup-sizing-lg" value="Random"
                                                   style="text-align: center;">
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <!-- Error -->
                        <div class="input-group input-group-lg"
                             style="padding-left: 30%; padding-right: 30%; margin: 0; text-align: center;">
                            <c:if test="${not empty error}">
                                <div class="Title"
                                     style="margin: 0; font-weight: 300; font-size: 1.8em; text-align: center; color: #d32f2f; font-size: 1.1em; width: 100%;">${error}</div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
    <%--<div class="FooterContainer">--%>
    <%--<footer class="Footer AppFooter">--%>
    <%--<p>Posted by: Weihao Li, Yifan Wang, Jing Zhang, Yingnan Li</p>--%>
    <%--<p>Contact information:<a href="yingnan.li@stonybrook.edu"> yingnan.li@stonybrook.edu</a>.</p>--%>
    <%--</footer>--%>
    <%--</div>--%>
    <%--<div class="CustomizeContainer">--%>
    <%--<div class="AppCustomize">--%>
    <%--<header class="CustomizeHeader">--%>
    <%--<h4 class="Title"> Customize Theme </h4>--%>
    <%--<a href="https://modular-admin-html.modularcode.io/" id="CustomizeDismissButton" class="Button -dismiss CustomizeDismissButton">--%>
    <%--<i class="fa fa-times Icon"></i>--%>
    <%--</a>--%>
    <%--</header>--%>
    <%--<div class="CustomizeContent"> </div>--%>
    <%--</div>--%>
    <%--</div>--%>
</div>
<script src="./ModularAdmin _ HTML version_files/bundle.js"></script>
</body>
</html>