<mvc:annotation-driven ignoreDefaultModelOnRedirect="true"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<html class="no-js gr__modular-admin-html_modularcode_io" lang="en" id="Main">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--<base href="/">-->
    <base href=".">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Celeste Jotto </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://modular-admin-html.modularcode.io/favicon.ico">
    <link rel="stylesheet" href="./ModularAdmin _ HTML version_files/css">
    <link rel="stylesheet" href="./ModularAdmin _ HTML version_files/bundle.css">
    <style type="text/css">/* Chart.js */
    @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}
    </style>
    <style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) ;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;box-sizing: content-box;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style>
    <script src="https://code.jquery.com/jquery-1.10.2.js"
            type="text/javascript"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/app-ajax.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            // document.body.onselectstart = function(){
            //     return false;
            // }
            $(".myclass").css("fontSize", 35);
            $(".myclass").click(function(){
                var color = $(this).css("color")
                //alert(color.toString())
                if(color=="rgb(0, 0, 0)") {
                    // code to be executed if condition is true
                    $(this).css('color', 'red');

                }
                else if(color=="rgb(255, 0, 0)"){
                    $(this).css('color', 'green');
                }
                else{
                    $(this).css('color', 'black');
                }
            })
        })
    </script>
</head>
<body data-gr-c-s-loaded="true">
<!--
   You may use the following felper classes on the .App element

   -dafault                    Default layout options

   -header-fixed               Fixed header on all viewports
   -header-fixed-mobile        Fixed header on all viewports
   -sidebar-compact-tablet     Compact sidebar on tablet viewports
   -sidebar-open-tablet        Open sidebar on tabler viewports
   -sidebar-compact-desktop    Compact sidebar on desktop viewports
   -customize-open             Open customize menu panel
   -->
<div id="App" class="App">
    <div class="HeaderContainer">
        <header class="AppHeader">
            <div class="HeaderBlock">
                <%--<h1 class="Button -primary ControlButton" id="ProfileButton">Celeste Jotto</h1>--%>
                <form method="get" action="login" style="margin: 0; padding: 0;">
                    <button type="submit" class="Button -primary ControlButton" style="color: #509C9C;">Celeste Jotto</button>
                </form>
            </div>
            <div class="HeaderBlock">
                <nav class="ProfileNav">
                    <div class="Dropdown NavItem Profile">
                        <%--<form method="get" action="login" style="margin: 0; padding: 0;">--%>
                            <%--<button type="submit" class="Button -primary ControlButton" id="AboutUsButton">Profile</button>--%>
                        <%--</form>--%>
                        <%--<form method="get" action="index" style="margin: 0; padding: 0;">--%>
                            <%--<button type="submit" class="Button -primary ControlButton" id="HeaderSearchToggleButto">--%>
                                <%--Logout--%>
                            <%--</button>--%>
                        <%--</form>--%>

                        <form method="post" action="/chooseWord" style="margin: 0; padding: 0;">
                            <div class="input-group input-group-lg">
                                <input hidden="hidden" name="founduser" value="${userInfo.username}">
                                <input type="submit" class="Button -primary ControlButton"
                                       aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" value="Start New" style="color: #509C9C;">
                            </div>
                        </form>
                        <c:if test="${currentGame.isActive()}">
                            <form action="/gamePage" method="post" style="margin: 0; padding: 0;">
                                <div class="input-group input-group-lg">
                                    <input hidden="hidden" name="giveUp" type="checkbox" checked="checked">
                                    <input type="submit" class="Button -primary ControlButton"
                                           aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" value="Give up" style="color: #509C9C;">
                                </div>
                            </form>
                        </c:if>
                    </div>
                </nav>
            </div>
        </header>
    </div>
    <br>
    <h2>${currentGame.getActualWordAI()}</h2>
    <h1 class="Title" style="margin: 1em 0; font-weight: 300; font-size: 1.5em; text-align: center;">
        Your secret word is: <b>${currentGame.getActualWordUser()}</b>
    </h1>
    <c:if test="${currentGame.isActive()}">
    <div style="font-size: 1em; margin-bottom: 10px; text-align: center;">

        <table class = "notesTable" style="color: black; width: 80%; margin: 1em 10%;">
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>
            <col width="3.8461538462%"/>

            <tr  onselectstart="return false">
                <td style="font-size: 1.4em;"><b>Notes:</b></td>

                <c:choose>
                <c:when test="${ca.get(0)==0}">
                    <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesA1').submit();">
                        <form id = "notesA1" action="/gamePage" method="post" style="margin: 0; padding: 0">
                            <input hidden="hidden" name="update" value="A">
                            <div style="font-size: 0.7em;">A</div>
                        </form>
                    </td>
                </c:when>
                <c:when test="${ca.get(0)==1}">
                    <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesA2').submit();"><form id = "notesA2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="A">A</form></td>
                </c:when>
                <c:otherwise>
                    <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesA3').submit();"><form id = "notesA3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="A">A</form></td>
                </c:otherwise>
            </c:choose>

                <c:choose>
                    <c:when test="${ca.get(1)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesB1').submit();"><form id = "notesB1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="B">B</form></td>
                    </c:when>
                    <c:when test="${ca.get(1)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesB2').submit();"><form id = "notesB2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="B">B</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesB3').submit();"><form id = "notesB3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="B">B</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(2)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesC1').submit();"><form id = "notesC1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="C">C</form></td>
                    </c:when>
                    <c:when test="${ca.get(2)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesC2').submit();"><form id = "notesC2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="C">C</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesC3').submit();"><form id = "notesC3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="C">C</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(3)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesD1').submit();"><form id = "notesD1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="D">D</form></td>
                    </c:when>
                    <c:when test="${ca.get(3)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesD2').submit();"><form id = "notesD2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="D">D</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesD3').submit();"><form id = "notesD3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="D">D</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(4)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesE1').submit();"><form id = "notesE1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="E">E</form></td>
                    </c:when>
                    <c:when test="${ca.get(4)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesE2').submit();"><form id = "notesE2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="E">E</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesE3').submit();"><form id = "notesE3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="E">E</form></td>
                    </c:otherwise>
                </c:choose>


                <c:choose>
                    <c:when test="${ca.get(5)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesF1').submit();"><form id = "notesF1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="F">F</form></td>
                    </c:when>
                    <c:when test="${ca.get(5)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesF2').submit();"><form id = "notesF2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="F">F</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesF3').submit();"><form id = "notesF3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="F">F</form></td>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${ca.get(6)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesG1').submit();"><form id = "notesG1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="G">G</form></td>
                    </c:when>
                    <c:when test="${ca.get(6)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesG2').submit();"><form id = "notesG2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="G">G</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesG3').submit();"><form id = "notesG3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="G">G</form></td>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${ca.get(7)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesH1').submit();"><form id = "notesH1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="H">H</form></td>
                    </c:when>
                    <c:when test="${ca.get(7)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesH2').submit();"><form id = "notesH2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="H">H</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesH3').submit();"><form id = "notesH3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="H">H</form></td>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${ca.get(8)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesI1').submit();"><form id = "notesI1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="I">I</form></td>
                    </c:when>
                    <c:when test="${ca.get(8)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesI2').submit();"><form id = "notesI2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="I">I</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesI3').submit();"><form id = "notesI3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="I">I</form></td>
                    </c:otherwise>
                </c:choose>


                <c:choose>
                    <c:when test="${ca.get(9)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesJ1').submit();"><form id = "notesJ1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="J">J</form></td>
                    </c:when>
                    <c:when test="${ca.get(9)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesJ2').submit();"><form id = "notesJ2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="J">J</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesJ3').submit();"><form id = "notesJ3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="J">J</form></td>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${ca.get(10)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesK1').submit();"><form id = "notesK1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="K">K</form></td>
                    </c:when>
                    <c:when test="${ca.get(10)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesK2').submit();"><form id = "notesK2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="K">K</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesK3').submit();"><form id = "notesK3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="K">K</form></td>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${ca.get(11)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesL1').submit();"><form id = "notesL1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="L">L</form></td>
                    </c:when>
                    <c:when test="${ca.get(11)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesL2').submit();"><form id = "notesL2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="L">L</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesL3').submit();"><form id = "notesL3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="L">L</form></td>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${ca.get(12)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesM1').submit();"><form id = "notesM1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="M">M</form></td>
                    </c:when>
                    <c:when test="${ca.get(12)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesM2').submit();"><form id = "notesM2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="M">M</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesM3').submit();"><form id = "notesM3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="M">M</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(13)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesN1').submit();"><form id = "notesN1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="N">N</form></td>
                    </c:when>
                    <c:when test="${ca.get(13)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesN2').submit();"><form id = "notesN2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="N">N</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesN3').submit();"><form id = "notesN3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="N">N</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(14)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesO1').submit();"><form id = "notesO1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="O">O</form></td>
                    </c:when>
                    <c:when test="${ca.get(14)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesO2').submit();"><form id = "notesO2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="O">O</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesO3').submit();"><form id = "notesO3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="O">O</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(15)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesP1').submit();"><form id = "notesP1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="P">P</form></td>
                    </c:when>
                    <c:when test="${ca.get(15)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesP2').submit();"><form id = "notesP2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="P">P</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesP3').submit();"><form id = "notesP3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="P">P</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(16)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesQ1').submit();"><form id = "notesQ1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="Q">Q</form></td>
                    </c:when>
                    <c:when test="${ca.get(16)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesQ2').submit();"><form id = "notesQ2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="Q">Q</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesQ3').submit();"><form id = "notesQ3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="Q">Q</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(17)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesR1').submit();"><form id = "notesR1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="R">R</form></td>
                    </c:when>
                    <c:when test="${ca.get(17)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesR2').submit();"><form id = "notesR2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="R">R</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesR3').submit();"><form id = "notesR3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="R">R</form></td>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${ca.get(18)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesS1').submit();"><form id = "notesS1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="S">S</form></td>
                    </c:when>
                    <c:when test="${ca.get(18)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesS2').submit();"><form id = "notesS2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="S">S</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesS3').submit();"><form id = "notesS3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="S">S</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(19)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesT1').submit();"><form id = "notesT1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="T">T</form></td>
                    </c:when>
                    <c:when test="${ca.get(19)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesT2').submit();"><form id = "notesT2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="T">T</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesT3').submit();"><form id = "notesT3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="T">T</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(20)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesU1').submit();"><form id = "notesU1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="U">U</form></td>
                    </c:when>
                    <c:when test="${ca.get(20)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesU2').submit();"><form id = "notesU2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="U">U</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesU3').submit();"><form id = "notesU3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="U">U</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(21)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesV1').submit();"><form id = "notesV1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="V">V</form></td>
                    </c:when>
                    <c:when test="${ca.get(21)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesV2').submit();"><form id = "notesV2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="V">V</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesV3').submit();"><form id = "notesV3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="V">V</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(22)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesW1').submit();"><form id = "notesW1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="W">W</form></td>
                    </c:when>
                    <c:when test="${ca.get(22)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesW2').submit();"><form id = "notesW2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="W">W</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesW3').submit();"><form id = "notesW3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="W">W</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(23)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesX1').submit();"><form id = "notesX1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="X">X</form></td>
                    </c:when>
                    <c:when test="${ca.get(23)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesX2').submit();"><form id = "notesX2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="X">X</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesX3').submit();"><form id = "notesX3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="X">X</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(24)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesY1').submit();"><form id = "notesY1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="Y">Y</form></td>
                    </c:when>
                    <c:when test="${ca.get(24)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesY2').submit();"><form id = "notesY2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="Y">Y</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesY3').submit();"><form id = "notesY3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="Y">Y</form></td>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${ca.get(25)==0}">
                        <td class="myClass" style="color: #000000;" onclick="document.getElementById('notesZ1').submit();"><form id = "notesZ1" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="Z">Z</form></td>
                    </c:when>
                    <c:when test="${ca.get(25)==1}">
                        <td class="myClass" style="color: #ff0000;" onclick="document.getElementById('notesZ2').submit();"><form id = "notesZ2" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="Z">Z</form></td>
                    </c:when>
                    <c:otherwise>
                        <td class="myClass" style="color: #00ff00;" onclick="document.getElementById('notesZ3').submit();"><form id = "notesZ3" action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;"><input hidden="hidden" name="update" value="Z">Z</form></td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </table>

    </div>

    </c:if>


    <h2 style="color: cadetblue; text-align: center;">${resultMsg} ${aiWord}</h2>
        <div class="SidebarOverlay" id="SidebarOverlay"></div>
        <div class="Content">
            <article class="Page Dashboard">
                <div class="PageContainer" style="padding-top:1em;">
                    <div class="MainTitle">
                        <c:if test="${currentGame.isActive()}">
                        <div class="Title" style="margin: 0; font-weight: 300; font-size: 1.6em; text-align: center;">
                            Enter Your Guess:
                        </div>

                            <div class="row -row-compact-sm -row-compact-md -row-compact-lg" style="width: 40%; margin: 0 30%;">
                                <table style="margin: 0 30%; width:40%; padding: 0;">
                                    <col width="80%"/>
                                    <col width="20%"/>
                                    <tr>
                                        <td>
                                            <form action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;">
                                                <div class="input-group input-group-lg" style="text-align: center">
                                                    <input type="text" class="form-control" name="userWord" required
                                                           value="${selectedWord}"
                                                           aria-label="Sizing example input"
                                                           aria-describedby="inputGroup-sizing-lg"
                                                           required style="text-align: center; width: 60%; margin-right: 1%;">

                                                    <input type="submit" class="form-control" aria-label="Sizing example input"
                                                           aria-describedby="inputGroup-sizing-lg" value="Submit"
                                                           style="text-align: center; width: 20%;">
                                                </div>
                                            </form>
                                        </td>
                                        <td>
                                            <form action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;">
                                                <div class="input-group input-group-lg" style="">
                                                    <input hidden="hidden" name="getRandom" type="checkbox" checked="checked">
                                                    <input type="submit" class="form-control" aria-label="Sizing example input"
                                                           aria-describedby="inputGroup-sizing-lg" value="Random"
                                                           style="text-align: center;">
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                </table>
                                <!-- Error -->
                                <div class="input-group input-group-lg"
                                     style="padding-left: 30%; padding-right: 30%; margin: 0; text-align: center;">
                                    <c:if test="${not empty error}">
                                        <div class="Title"
                                             style="margin: 0; font-weight: 300; font-size: 1.8em; text-align: center; color: #d32f2f; font-size: 1.1em; width: 100%;">${error}</div>
                                    </c:if>
                                </div>
                            </div>

                        <%--<form action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;">--%>
                            <%--<div class="input-group input-group-lg" style="padding-left: 30%; padding-right: 30%; margin-top: 10px; margin-bottom: 10px;">--%>
                                <%--<input type="text" name="userWord" value="${selectedWord}" placeholder="apple" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">--%>
                            <%--</div>--%>
                            <%--<div class="input-group input-group-lg" style="padding-left: 46%; padding-right: 46%; margin-top: 10px; margin-bottom: 10px;">--%>
                                <%--<input type="submit" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" value="Submit">--%>
                            <%--</div>--%>
                        <%--</form>--%>


                        <%--<form action="/gamePage" method="post" style="margin: 0; padding: 0; font-size: 0.7em;">--%>
                            <%--<div class="input-group input-group-lg" style="padding-left: 46%; padding-right: 46%; margin-top: 10px; margin-bottom: 30px;">--%>
                                <%--<input hidden="hidden" name="getRandom" type="checkbox" checked="checked">--%>
                                <%--<input type="submit" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" value="Random">--%>
                            <%--</div>--%>
                            <%--</form>--%>



                        </c:if>

                        <table style="width: 80%; margin: 1em 10%;">
                            <tr>
                                <th>Round</th>
                                <th>AI Guess</th>
                                <th>AI Score</th>
                                <th>Your Guess</th>
                                <th>Your Score</th>
                            </tr>
                            <c:forEach items="${currentGame.gameLog}" var="round" varStatus="loopCounter">
                                <tr >
                                    <td><c:out value="${round.round}"/></td>
                                    <td><c:out value="${round.aiRecord.word}"/></td>
                                    <td><c:out value="${round.aiRecord.score}"/></td>
                                    <td><c:out value="${round.userRecord.word}"/></td>
                                    <td><c:out value="${round.userRecord.score}"/></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </article>

        </div>




</div>
<!-- <script src="./ModularAdmin _ HTML version_files/bundle.js"></script> -->


</body>
</html>