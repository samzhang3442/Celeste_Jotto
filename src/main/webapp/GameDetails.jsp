<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%--<!DOCTYPE html>--%>
<!-- saved from url=(0042)https://modular-admin-html.modularcode.io/ -->
<html class="no-js gr__modular-admin-html_modularcode_io" lang="en" id="Main"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!--<base href="/">--><base href=".">
    
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Celeste Jotto </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://modular-admin-html.modularcode.io/favicon.ico">
    <link rel="stylesheet" href="./ModularAdmin _ HTML version_files/css">
    <link rel="stylesheet" href="./ModularAdmin _ HTML version_files/bundle.css"> <style type="text/css">/* Chart.js */


  @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style><style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0);background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;box-sizing: content-box;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>
  <body data-gr-c-s-loaded="true">
    <!--
  You may use the following felper classes on the .App element

  -dafault                    Default layout options

  -header-fixed               Fixed header on all viewports
  -header-fixed-mobile        Fixed header on all viewports
  -sidebar-compact-tablet     Compact sidebar on tablet viewports
  -sidebar-open-tablet        Open sidebar on tabler viewports
  -sidebar-compact-desktop    Compact sidebar on desktop viewports
  -customize-open             Open customize menu panel
-->
    <div id="App" class="
    App
  ">
      <div class="HeaderContainer">
        <header class="AppHeader">
          <div class="HeaderBlock">

            <form method="get" action="login" style="margin: 0; padding: 0;">
              <button class="Button -primary ControlButton" style="color: #509C9C;">Celeste Jotto</button>
            </form>

          </div>
          <div class="HeaderBlock">
            <nav class="ProjectNav">
              <!--<div class="NavBarTitle">%{username}</div>-->
            </nav>
          </div>
          <div class="HeaderBlock">
            <nav class="ProfileNav">
              <div class="Dropdown NavItem Profile">
                  <button onclick="location.href = '/login';" class="Button -primary ControlButton" id="HeaderSidebarToggleButton" style="color: #509C9C;">
                      Profile
                  </button>
                <button onclick="location.href = '/logout';" class="Button -primary ControlButton" id="HeaderSearchToggleButto" style="color: #509C9C;">
				 Logout
				</button>
				
              </div>
            </nav>
          </div>
        </header>
      </div>
      <div class="SidebarOverlay" id="SidebarOverlay"></div>
      <div class="ContentContainer Dashboard">
        <div class="Content">
          <article class="Page Dashboard">
            <div class="PageContainer">
              <div class="Intro">
                <h1 class="Title"> Game Details </h1>
              </div>
              <div class="row -row-compact-sm -row-compact-md -row-compact-lg">        
			  <div class="row -row-compact-sm -row-compact-md -row-compact-lg">
                <!--<div class="col-xl-8 -sameheight">-->
                  <div class="Card Items" id="DashboardItems">
                      <div class="ResponsiveList -striped -bordered -sm">
                          <div class="ListHeader -hidden-xs -hidden-sm">
                              <div class="ItemCol ColStats -rel-size-2 -size-xs-12 ColStats">
                                  <h5 style="color: #7c7c7c">AI's Secret Word: ${AIword}</h5>
                              </div>
                              <div class="ItemCol ColStats -rel-size-2 -size-xs-12 ColStats">
                                  <h5 style="color: #7c7c7c">Your Secret Word: ${Userword}</h5>
                              </div>
                      </div>
                    </div>
                    <div class="ResponsiveList -striped">
                      <div class="ListHeader -hidden-xs -hidden-sm">
                        <div class="ItemCol ColStats -rel-size-2 -size-xs-12 ColStats"> Your Guesses </div>
                        <div class="ItemCol ColStats -rel-size-2 -size-xs-12 ColStats"> Your Score </div>
                        <div class="ItemCol ColStats -rel-size-2 -size-xs-12 ColStats"> AI Guesses </div>
                        <div class="ItemCol ColStats -rel-size-2 -size-xs-12 ColStats"> AI Score </div>

                      </div>
                      <div class="ListContent">


                          <c:if test="${not empty gamedetail}">
                            <%--<table class="table table-sm">--%>
                              <!-- iterate over the collection using forEach loop -->
                              <c:forEach var="detail" items="${gamedetail}">
                                <!-- create an html table row -->
                                <div class="ListItem">
                                  <div class="Divider -visible-xs"> </div>
                                  <div class="ItemCol ColStats -rel-size-2">
                                    <div class="ItemValue">
                                    <%--col 1--%>
                                  <%--<td>--%>
                                    <c:choose>
                                      <c:when test="${fn:contains(AIword,detail.charAt(1) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(1)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(1)}</a>
                                      </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                      <c:when test="${fn:contains(AIword,detail.charAt(2) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(2)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(2)}</a>
                                      </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                      <c:when test="${fn:contains(AIword,detail.charAt(3) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(3)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(3)}</a>
                                      </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                      <c:when test="${fn:contains(AIword,detail.charAt(4) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(4)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(4)}</a>
                                      </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                      <c:when test="${fn:contains(AIword,detail.charAt(5) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(5)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(5)}</a>
                                      </c:otherwise>
                                    </c:choose>
                                  <%--</td>--%>
                                  </div>
                                  </div>
                                    <%--col2--%>
                            <div class="Divider -visible-xs"> </div>
                            <div class="ItemCol ColStats -rel-size-2">
                              <div class="ItemValue">
                                  ${detail.charAt(6)}
                              </div>
                            </div>



                                    <%--col3--%>
                                  <div class="Divider -visible-xs"> </div>
                                  <div class="ItemCol ColStats -rel-size-2">
                                    <div class="ItemValue">
                                    <c:choose>
                                      <c:when test="${fn:contains(Userword,detail.charAt(7) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(7)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(7)}</a>
                                      </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                      <c:when test="${fn:contains(Userword,detail.charAt(8) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(8)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(8)}</a>
                                      </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                      <c:when test="${fn:contains(Userword,detail.charAt(9) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(9)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(9)}</a>
                                      </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                      <c:when test="${fn:contains(Userword,detail.charAt(10) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(10)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(10)}</a>
                                      </c:otherwise>
                                    </c:choose>

                                    <c:choose>
                                      <c:when test="${fn:contains(Userword,detail.charAt(11) )}">
                                        <a style="color: #18dbdb;">${detail.charAt(11)}</a>
                                      </c:when>
                                      <c:otherwise>
                                        <a>${detail.charAt(11)}</a>
                                      </c:otherwise>
                                    </c:choose>

                                    </div>
                                  </div>

                                    <%--col4--%>
                                  <div class="Divider -visible-xs"> </div>
                                  <div class="ItemCol ColStats -rel-size-2">
                                    <div class="ItemValue">
                                    ${detail.charAt(12)}
                                    </div>
                                  </div>
                                </div>
                              </c:forEach>
                            </table>
                          </c:if>
					   
					   
					   
                      </div>
                      <div class="ListFooter"> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           </article>
        </div>
      </div>
        <%--<div class="FooterContainer">--%>
        <%--<footer class="Footer AppFooter">--%>
        <%--<p>Posted by: Weihao Li, Yifan Wang, Jing Zhang, Yingnan Li</p>--%>
        <%--<p>Contact information:<a href="yingnan.li@stonybrook.edu"> yingnan.li@stonybrook.edu</a>.</p>--%>
        <%--</footer>--%>
        <%--</div>--%>
        <%--<div class="CustomizeContainer">--%>
        <%--<div class="AppCustomize">--%>
        <%--<header class="CustomizeHeader">--%>
        <%--<h4 class="Title"> Customize Theme </h4>--%>
        <%--<a href="https://modular-admin-html.modularcode.io/" id="CustomizeDismissButton" class="Button -dismiss CustomizeDismissButton">--%>
        <%--<i class="fa fa-times Icon"></i>--%>
        <%--</a>--%>
        <%--</header>--%>
        <%--<div class="CustomizeContent"> </div>--%>
        <%--</div>--%>
        <%--</div>--%>
        <%--</div>--%>
    <script src="./ModularAdmin _ HTML version_files/bundle.js"></script>
  
</body></html>