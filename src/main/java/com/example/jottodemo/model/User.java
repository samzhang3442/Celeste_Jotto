package com.example.jottodemo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class User implements Serializable {
    @Id
    @Column(name = "username", nullable = false, length = 32)
    @NotBlank
    private String username;

    @Column(nullable = false)
    @NotBlank
    private String password;


    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate

//    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = false)
//    private List<GameHis> games = new ArrayList<>();

    private Date updatedAt;

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }



    public String getPassword() {
        return password;
    }



    public String getUsername() {
        return username;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }



    public void setPassword(String password) {
        this.password = password;
    }


    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setUsername(String user_name) {
        this.username = user_name;
    }
}
