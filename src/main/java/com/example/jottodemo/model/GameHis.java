package com.example.jottodemo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GameHis")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt"},allowGetters = true)
public class GameHis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gameid;

    @Lob
    @Column(nullable = false, updatable = false, length = 100000)
    private String Game_Details;

    @Column(name = "username", nullable = false, updatable = false)
    private String username;


    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false, updatable = false)
    private String Game_Result;

    @Column(nullable = false, updatable = false)
    private String AIword;

    @Column(nullable = false, updatable = false)
    private String Userword;

    public void setUserword(String userword) {
        Userword = userword;
    }

    public void setAIword(String AIword) {
        this.AIword = AIword;
    }

    public String getUserword() {
        return Userword;
    }

    public String getAIword() {
        return AIword;
    }

    public String getGame_Result() {
        return Game_Result;
    }

    public void setGame_Result(String game_Result) {
        Game_Result = game_Result;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getGameid() {
        return gameid;
    }

    public void setGameid(Long gameid) {
        gameid = gameid;
    }

    public String getGame_Details() {
        return Game_Details;
    }

    public void setGame_Details(String game_Details) {
        Game_Details = game_Details;
    }

    public String getUsername() { return username;}

    public void setUsername(String name) {
        username = name;
    }

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "username" ,nullable = false)
//    @OnDelete(action = OnDeleteAction.CASCADE)
//    @JsonIgnore
//    private User user;
//
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
}
