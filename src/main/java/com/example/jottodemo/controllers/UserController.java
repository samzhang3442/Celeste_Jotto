package com.example.jottodemo.controllers;

import com.example.jottodemo.exception.ResourceNotFoundException;
import com.example.jottodemo.model.GameHis;
import com.example.jottodemo.model.User;
import com.example.jottodemo.repository.GameHisRepository;
import com.example.jottodemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    GameHisRepository gameHisRepository;



    // Get All Users
//    @GetMapping("/userslist")
//    @ResponseBody
//    public List<User> getAllUsers() {
//        return userRepository.findAll();
//    }

    @RequestMapping("/logout")
    public String Logout(HttpServletRequest request){

        if(request.getSession().getAttribute("username")!=null){
            request.getSession().removeAttribute("username");
        }

        return "index";
    }


    // Create a new User
    @RequestMapping(value = "/welcome", method = RequestMethod.POST)
    public String createUser(@RequestParam(value = "username") String username,
                             @RequestParam(value = "password") String password,
                             @RequestParam(value = "passwordConfirm") String passwordConfirm,
                             HttpServletRequest request, ModelMap model) {

        if (username.length() < 6 || username.length() > 32) {
            model.addAttribute("errors","Please use between 6 and 32 characters for username.");
            return "signup";
        }

        if (password.length() < 8 || password.length() > 32) {
            model.addAttribute("errors","Try use between 8 and 32 characters for password.");
            return "signup";
        }

        if (!passwordConfirm.equals(password)) {
            model.addAttribute("errors","These passwords don't match.");
            return "signup";
        }

        if (userRepository.existsById(username)) {
            model.addAttribute("errors","Someone already has that username.");
            return "signup";
        }

        User user = new User();
        user.setUsername(username);
        user.setPassword(password);


        userRepository.save(user);

        User founduser = userRepository.findById(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

        List<GameHis> games = gameHisRepository.findByUsername(username);
        Collections.sort(games, new Comparator<GameHis>() {
            @Override
            public int compare(GameHis o1, GameHis o2) {
                return -o1.getCreatedAt().toString().compareTo(o2.getCreatedAt().toString());
            }
        });


        model.addAttribute("games",games);

        model.addAttribute("founduser",founduser);
        request.getSession().setAttribute("username",username);

        return "/profile";
    }

    // normal returo to profile page
    @GetMapping("/login")
    public String getUserByUsername(
                                    ModelMap model, HttpServletRequest request) {
        if(request.getSession().getAttribute("username")==null){
            return "index";
        }
        String username =(String) request.getSession().getAttribute("username");

        User founduser = userRepository.findById(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

        List<GameHis> games = gameHisRepository.findByUsername(username);
        Collections.sort(games, new Comparator<GameHis>() {
            @Override
            public int compare(GameHis o1, GameHis o2) {
                return -o1.getCreatedAt().toString().compareTo(o2.getCreatedAt().toString());
            }
        });


        model.addAttribute("games",games);

        model.addAttribute("founduser",founduser);
        request.getSession().setAttribute("username",username);
        return "/profile";


    }

    //First time login and check for username and password
    @PostMapping("/firstlogin")
    public String FirstLogin(@RequestParam(value = "username" ,required = false) String username,
                             @RequestParam(value = "password" ,required = false) String password,
                             ModelMap model, HttpServletRequest request) {
        if(request.getSession().getAttribute("username")!=null){
            request.getSession().removeAttribute("username");
        }
        if(username.isEmpty()){
            model.addAttribute("errors","This username can't be empty.");
            return "index";
        }
        if(password.isEmpty()){
            model.addAttribute("errors","This password can't be empty.");
            return "index";
        }
        if (!userRepository.existsById(username)) {
            model.addAttribute("errors","This username doesn't exist.");
            return "index";
        }
        User founduser = userRepository.findById(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
        if(!password.equals(founduser.getPassword())){
            model.addAttribute("errors","Your password is incorrect.");
            return "index";
        }
        List<GameHis> games = gameHisRepository.findByUsername(username);
        Collections.sort(games, new Comparator<GameHis>() {
            @Override
            public int compare(GameHis o1, GameHis o2) {
                return -o1.getCreatedAt().toString().compareTo(o2.getCreatedAt().toString());
            }
        });
        model.addAttribute("games",games);
        model.addAttribute("founduser",founduser);
        request.getSession().setAttribute("username",username);
        return "/profile";


    }

    // Update a User
//    @PutMapping("/users/{username}")
//    public User updateUser(@PathVariable(value = "username") String username,
//                           @Valid @RequestBody User userDetails) {
//
//        User user = userRepository.findById(username)
//                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
//
//        user.setUsername(userDetails.getUsername());
//        user.setPassword(userDetails.getPassword());
//
//        User updatedUser = userRepository.save(user);
//        return updatedUser;
//    }
    // Delete a User
//    @DeleteMapping("/users/{username}")
//    public ResponseEntity<?> deleteUser(@PathVariable(value = "username") String username) {
//        User user = userRepository.findById(username)
//                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
//
//        userRepository.delete(user);
//
//        return ResponseEntity.ok().build();
//    }

}