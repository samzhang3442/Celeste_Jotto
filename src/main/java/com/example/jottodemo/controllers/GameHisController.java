package com.example.jottodemo.controllers;

import com.example.jottodemo.exception.ResourceNotFoundException;
import com.example.jottodemo.model.GameHis;
import com.example.jottodemo.repository.GameHisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
//@RestController
public class GameHisController {
    @Autowired
    GameHisRepository gameHisRepository;





    // Create a new GameHistory
//    @PostMapping("/createNewHis")
//    public GameHis createGameHistory(@Valid @RequestBody GameHis GameHistory) {
//        return gameHisRepository.save(GameHistory);
//    }

    // Get a Single GameHistory
    @PostMapping("/GameHistory")
    public String getGameHistoryByGameID(@RequestParam(value = "gid") Long gid, ModelMap model,HttpServletRequest request) {

        if (request.getSession().getAttribute("username") == null) {
            return "index";
        }


        GameHis gamehis = gameHisRepository.findById(gid)
                .orElseThrow(() -> new ResourceNotFoundException("GameHistory", "GameID", gid));

        if (!request.getSession().getAttribute("username").equals(gamehis.getUsername())) {
            return "index";
        }

        String[] gamedetial = gamehis.getGame_Details().split(",");
        for(int i=0;i<gamedetial.length;i++)
            gamedetial[i] = gamedetial[i].replaceAll(";","");


        model.addAttribute("gamedetail",gamedetial);
        model.addAttribute("AIword",gamehis.getAIword());
        model.addAttribute("Userword",gamehis.getUserword());


        return "GameDetails";

    }
    // Update a GameHistory
//    @PutMapping("/GameHistory/{GameID}")
//    public GameHis updateGameHistory(@PathVariable(value = "GameID") Long GameID,
//                                         @Valid @RequestBody GameHis GameHistoryDetails) {
//
//        GameHis GameHistory = gameHisRepository.findById(GameID)
//                .orElseThrow(() -> new ResourceNotFoundException("GameHistory", "GameID", GameID));
//
//        GameHistory.setGameid(GameHistoryDetails.getGameid());
//        GameHistory.setGame_Details(GameHistoryDetails.getGame_Details());
//
//        GameHis updatedGameHistory = gameHisRepository.save(GameHistory);
//        return updatedGameHistory;
//    }

    // Delete a GameHistory
//    @DeleteMapping("/GameHistory/{GameID}")
//    public ResponseEntity<?> deleteGameHistory(@PathVariable(value = "GameID") Long GameID) {
//        GameHis GameHistory = gameHisRepository.findById(GameID)
//                .orElseThrow(() -> new ResourceNotFoundException("GameHistory", "GameID", GameID));
//
//        gameHisRepository.delete(GameHistory);
//
//        return ResponseEntity.ok().build();
//    }
}
