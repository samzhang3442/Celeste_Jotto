package com.example.jottodemo.controllers;

import com.example.jottodemo.exception.InvalidWordLengthException;
import com.example.jottodemo.jotto.JottoGame;
import com.example.jottodemo.jotto.PlayerType;
import com.example.jottodemo.model.GameHis;
import com.example.jottodemo.model.User;
import com.example.jottodemo.repository.GameHisRepository;
import com.example.jottodemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Random;

@Controller
public class GamePageController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    GameHisRepository gamaHisRepository;


    @RequestMapping("/gamePage")
    public String gamePage(@RequestParam(value = "getRandom", required = false) boolean rand,
                           @RequestParam(value = "userWord", required = false) String userWord,
                           @RequestParam(value = "giveUp", required = false) boolean giveUp,
                           @RequestParam(value = "update", required = false) String s,
                           ModelMap model, HttpServletRequest request) {

        // Remove unnecessary
        model.remove("error");
        model.remove("selectedWord");
        model.remove("aiWord");
        model.remove("resultMsg");


        // Ensure user has logged in

        if (s!=null) {
            ArrayList<Integer> ca = (ArrayList<Integer>) request.getSession().getAttribute("ca");
            int i = s.charAt(0) - 'A';
            int j = (ca.get(i) + 1) % 3;
            ca.set(i, j);
            //System.out.println(s+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            return "gamePage";
        } else {
            if (request.getSession().getAttribute("userInfo") == null) {
                return "index";
            }

            // Get Current Active Game
            JottoGame game = (JottoGame) request.getSession().getAttribute("currentGame");
            if (game == null || !game.isActive()) {
                return "chooseWord";
            }


            if (giveUp) {
                game.giveUp();
                model.addAttribute("aiWord", "AI Word is: " + game.getActualWordAI());
            } else if (rand) {
                model.addAttribute("selectedWord", game.getWordUser());
                userWord = game.getWordUser();
            } //else
            if (userWord != null) {
                try {
                    // User Turn
                    int score = game.playUser(userWord);
                    if (score < 0) {
                        model.addAttribute("error", "Invalid Word!");
                        return "gamePage";
                    }

                    if (game.isWinning(userWord, PlayerType.USER)) {

                        /////
                        game.setactive();
                        final String aiGuess = game.getWordAI();
                        int aiScore = game.playAI(aiGuess);
                        while (aiScore < 0) {
                            aiScore = game.playAI(aiGuess);
                        }
                        game.setInactive();
                        /////

                        model.addAttribute("resultMsg", JottoGame.MSG_USER_WIN);
                        User user = (User) request.getSession().getAttribute("userInfo");
                        GameHis finalgame = new GameHis();
                        finalgame.setGame_Result("Win");
                        finalgame.setUsername(user.getUsername());
                        finalgame.setGame_Details(game.gameLog.toString());
                        finalgame.setAIword(game.getActualWordAI());
                        finalgame.setUserword(game.getActualWordUser());
                        gamaHisRepository.save(finalgame);
                        return "gamePage";


                    }

                Random rn = new Random();
                int min = 25;
                int range = 28 - min + 1;
                int randomNum = rn.nextInt(range) + min;
                if (game.gameLog.size() >= randomNum) {

                    final String aiGuess = game.getActualWordUser();
                    int aiScore = game.playAI(aiGuess);
                    if (game.isWinning(aiGuess, PlayerType.AI)) {
                        model.addAttribute("resultMsg", JottoGame.MSG_AI_WIN);
                        User user = (User) request.getSession().getAttribute("userInfo");
                        GameHis finalgame = new GameHis();
                        finalgame.setGame_Result("Lose");
                        finalgame.setGame_Details(game.gameLog.toString());
                        finalgame.setUsername(user.getUsername());
                        finalgame.setAIword(game.getActualWordAI());
                        finalgame.setUserword(game.getActualWordUser());
                        gamaHisRepository.save(finalgame);
                        model.addAttribute("aiWord", "AI Word is: " + game.getActualWordAI());
                        return "gamePage";
                    }

                }
                    // AI Turn
                    final String aiGuess = game.getWordAI();
                    int aiScore = game.playAI(aiGuess);
                    while (aiScore < 0) {
                        aiScore = game.playAI(aiGuess);
                    }
                    if (game.isWinning(aiGuess, PlayerType.AI)) {
                        model.addAttribute("resultMsg", JottoGame.MSG_AI_WIN);
                        User user = (User) request.getSession().getAttribute("userInfo");
                        GameHis finalgame = new GameHis();
                        finalgame.setGame_Result("Lose");
                        finalgame.setGame_Details(game.gameLog.toString());
                        finalgame.setUsername(user.getUsername());
                        finalgame.setAIword(game.getActualWordAI());
                        finalgame.setUserword(game.getActualWordUser());
                        gamaHisRepository.save(finalgame);
                        model.addAttribute("aiWord", "AI Word is: " + game.getActualWordAI());
                    }


                } catch (InvalidWordLengthException ex) {
                    model.addAttribute("error", "Only 5-letter Word Allowed");
                }
                return "gamePage";
            }
            return "gamePage";
        }
    }

}
