package com.example.jottodemo.controllers;

import com.example.jottodemo.exception.InvalidWordException;
import com.example.jottodemo.exception.InvalidWordLengthException;
import com.example.jottodemo.exception.ResourceNotFoundException;
import com.example.jottodemo.jotto.JottoGame;
import com.example.jottodemo.model.User;
import com.example.jottodemo.repository.UserRepository;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@Controller
public class ChooseWordController {

    @Autowired
    UserRepository userRepository;


    @RequestMapping("/chooseWord")
    public String chooseWord(
                             @RequestParam(value = "getRandom", required = false) boolean rand,
                             @RequestParam(value = "userWord", required = false) String userWord,
                             ModelMap model, HttpServletRequest request) {
        String name = request.getSession().getAttribute("username").toString();
        System.out.println(name);
        System.out.println("Dao le -" + rand);
        User founduser = userRepository.findById(name)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", name));

        // Remove Unnecessary Attributes
        model.remove("error");
        request.getSession().removeAttribute("currentGame");

        // Handle Redirecting Cases
        if (rand) { // Choose Random Word
            while (true) {
                try {
                    JottoGame game = new JottoGame("");
                    request.getSession().setAttribute("currentGame", game);
                    ArrayList<Integer> ca = new ArrayList<>();
                    for(int i=0;i<26;i++){
                        ca.add(0);
                    }
                    request.getSession().setAttribute("ca", ca);
                    break;
                } catch (Exception ex) {
                    //model.addAttribute("error", "Unexpected Error Occurred (InitGame)");
                }
            }
        } else if (userWord != null) {
            try {
                JottoGame game = new JottoGame(userWord);
                request.getSession().setAttribute("currentGame", game);
                ArrayList<Integer> ca = new ArrayList<>();
                for(int i=0;i<26;i++){
                    ca.add(0);
                }
                request.getSession().setAttribute("ca", ca);
                return "gamePage";
            } catch (InvalidWordException ex) {
                model.addAttribute("error", "Invalid Word!");
            } catch (InvalidWordLengthException ex1) {
                model.addAttribute("error", "Only 5-letter Word Allowed");
            }
        }

        model.addAttribute("founduser", founduser);
        request.getSession().setAttribute("userInfo", founduser);

        return "chooseWord";
    }

}
