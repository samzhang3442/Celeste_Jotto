package com.example.jottodemo.repository;

import com.example.jottodemo.model.GameHis;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface GameHisRepository extends JpaRepository<GameHis, Long> {

    List<GameHis> findByUsernameAndGameid(String username, Long gameid);
    List<GameHis> findByUsername(String username);
}
