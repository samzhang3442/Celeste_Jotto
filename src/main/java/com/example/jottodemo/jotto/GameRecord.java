package com.example.jottodemo.jotto;

public class GameRecord {
    final String word;
    final int score;

    GameRecord(String word, int score) {
        this.score = score;
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public int getScore() {
        return score;
    }
}