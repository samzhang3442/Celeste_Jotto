package com.example.jottodemo.jotto;

public class GameRound {
    public GameRecord userRecord;
    public GameRecord aiRecord;
    public int round;

    public GameRecord getUserRecord() {
        return userRecord;
    }

    public GameRecord getAiRecord() {
        return aiRecord;
    }

    public int getRound() {
        return round;
    }

    public String toString(){
        String s = "";
        s+= userRecord.word;
        s+=";";
        s+= userRecord.score;
        s+=";";
        s+=aiRecord.word;
        s+=";";
        s+=aiRecord.score;
        return s;
    }
}
