package com.example.jottodemo.jotto;

import com.example.jottodemo.exception.InvalidWordException;

public class JottoDriver {

    static int totalGame = 10;
    static int roundSum = 0;
    public static JottoGame currentGame;

    private static final String PATH = "words";
    public static void main(String[] args) throws Exception {
        /*BufferedReader br = new BufferedReader(new FileReader(new File(PATH)));
        StringBuffer sb = new StringBuffer();
        sb.append("public class WordList {\n");
        sb.append("\tpublic static final String[] DICTIONARY = {");
        while (true) {
            final String word = br.readLine();
            if (word == null)
                break;
            sb.append("\n\t\t\"").append(word).append("\"").append(",");
        }
        sb.replace(sb.length() - 1, sb.length(), "");
        sb.append("\n\t};");
        sb.append("\n}");
        PrintWriter pw = new PrintWriter(new File("WordList.java"));
        pw.print(sb);
        pw.close();
        */

        //Scanner input = new Scanner(System.in);
        for (int i = 0; i < totalGame; i++) {
            play();
        }

        System.out.println();
        System.out.println("Average: ");
        System.out.println((double) roundSum / (double) totalGame);

    }

    public static void play() throws Exception {
        String userWord = "";
        JottoGame game;
        do {
            System.out.print("Enter your 5-letter word: ");
            //userWord = input.next().toLowerCase();
            try {
                //game = new JottoGame(userWord);
                game = new JottoGame("");
                break;
            } catch (InvalidWordException ex) {

            }
        } while (true);
        System.out.println(game.getActualWordUser());
        int round = 0;
        while (true) {
            // User
            System.out.print("Enter your guess: ");
            String guess = "r";//input.next().toLowerCase();
            if (guess.equals("r"))
                guess = game.getWordUser();
            int userScore = game.playUser(guess);
            if (userScore < 0) {
                System.out.println("Invalid Word.");
                continue;
            }
            System.out.println("USER: " + guess + "\t" + userScore);
            if (game.isWinning(guess, PlayerType.USER)) {
                System.out.println("YOU WON!!!!!");
                break;
            }
            // AI
            final String aiGuess = game.getWordAI();
            int aiScore = game.playAI(aiGuess);
            while (aiScore < 0) {
                aiScore = game.playAI(aiGuess);
            }
            if (game.isWinning(aiGuess, PlayerType.AI)) {
                System.out.println("YOU LOST!!!!!");
                break;
            }
            System.out.println("\n-------------------------------------- " + (++round) + "\n");
        }
        roundSum += round;
    }
}
