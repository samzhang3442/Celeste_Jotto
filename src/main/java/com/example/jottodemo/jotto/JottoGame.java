package com.example.jottodemo.jotto;

import com.example.jottodemo.exception.InvalidWordException;
import com.example.jottodemo.exception.InvalidWordLengthException;

import java.util.ArrayList;

public class JottoGame {

    public static final String MSG_USER_WIN = "Congratulations! YOU WON!!";
    public static final String MSG_AI_WIN = "What a pity! You Lost.";

    private JottoPlayer ai;
    private JottoPlayer user;

    public JottoPlayer getUser() {
        return user;
    }

    private boolean active;

    public void setInactive() {
        active = false;
    }

    public void setactive() {
        active = true;
    }

    public ArrayList<GameRound> gameLog;

    public JottoGame(String userWord) throws InvalidWordException, InvalidWordLengthException {
        user = new JottoPlayer(userWord, PlayerType.USER);
        while (true) {
            try {
                ai = new JottoPlayer("", PlayerType.AI);
                break;
            } catch (Exception ex) {

            }
        }
        //log = new ArrayList<>();
        gameLog = new ArrayList<>();
        active = true;
    }

    public int playUser(String userGuess) throws InvalidWordLengthException {
        if (!active)
            return -2;
        if (!user.useWord(userGuess))
            return -1;
        final int score = ai.getOpponentScore(userGuess);
        // Logging
        //log.add(new GameRecord(userGuess, score));
        GameRound round = new GameRound();
        round.userRecord = new GameRecord(userGuess, score);
        round.round = gameLog.size() + 1;
        gameLog.add(0, round);
        return score;
    }

    public int playAI(String aiGuess) throws InvalidWordLengthException {
        if (!active)
            return -2;
        if (!ai.useWord(aiGuess))
            return -1;
        int score = user.getOpponentScore(aiGuess);
        ai.updateDictionary(aiGuess, score);
        // Logging
        //log.add(new GameRecord(aiGuess, score));
        gameLog.get(0).aiRecord = new GameRecord(aiGuess, score);
        System.out.println("A  I: " + aiGuess + "\t" + score);
        return score;
    }

    public String getWordUser() {
        return user.getRandomWord();
    }

    public String getWordAI() {
        return ai.getRandomWord();
    }

    public String getActualWordUser() {
        return user.getWord();
    }

    public String getActualWordAI() {
        return ai.getWord();
    }

    public ArrayList<GameRound> getGameLog() {
        return gameLog;
    }

    public boolean isWinning(String guess, PlayerType currentPlayer) {
        boolean winning = false;
        switch (currentPlayer) {
            case USER:
                winning = ai.isCorrectGuess(guess);
                break;
            case AI:
                winning = user.isCorrectGuess(guess);
                break;
        }
        if (winning)
            active = false;
        return winning;
    }

    public void giveUp() {
        setInactive();
    }

    public boolean isActive() {
        return active;
    }
}
