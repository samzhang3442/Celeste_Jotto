package com.example.jottodemo.jotto;

import com.example.jottodemo.exception.InvalidWordException;
import com.example.jottodemo.exception.InvalidWordLengthException;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class JottoPlayer {

    private final String word;
    private final PlayerType type;

    //private static final String PATH = "words";
    private Set<String> dictionary = new TreeSet<>();
    private Set<String> repeatingLetterWord = new TreeSet<>();
    private Set<String> used = new TreeSet<>();
    private ArrayList<GameRecord> history = new ArrayList<>();

    private Set<Character> mustContain = new TreeSet<>();
    private Set<Character> notContain = new TreeSet<>();

    JottoPlayer(String word, PlayerType type) throws InvalidWordException, InvalidWordLengthException {
        this.type = type;
        word = word.toLowerCase();
        setUpDictionary();
        if (type == PlayerType.AI || word.equals("")) {
            word = getRandomSecretWord();
            System.out.println("###\t" + word + "\t###");
        }
        if (!isValidWord(word))
            throw new InvalidWordException();
        for (int i = 0; i < word.length(); i++) {
            char ch = word.charAt(i);
            if (word.indexOf(ch) != word.lastIndexOf(ch))
                throw new InvalidWordException();
        }
        this.word = word;
    }

    private void setUpDictionary() {
        try {
            /*BufferedReader br = new BufferedReader(new FileReader(new File(PATH)));
            while (true) {
                String word = br.readLine();
                if (word == null)
                    break;
                dictionary.add(word);
                if (type == PlayerType.AI) {
                    boolean isUnique = true;
                    for (int i = 0; i < word.length(); i++) {
                        char ch = word.charAt(i);
                        String copy = word.replaceAll("" + ch, "");
                        if (copy.length() < 3) {
                            isUnique = false;
                            break;
                        }
                    }
                    if (!isUnique)
                        repeatingLetterWord.add(word);
                }
            }*/
            dictionary.clear();
            repeatingLetterWord.clear();
            for (final String word : WordList.DICTIONARY) {
                dictionary.add(word);
                if (type == PlayerType.AI) {
                    boolean isUnique = true;
                    for (int i = 0; i < word.length(); i++) {
                        char ch = word.charAt(i);
                        String copy = word.replaceAll("" + ch, "");
                        if (copy.length() < 3) {
                            isUnique = false;
                            break;
                        }
                    }
                    if (!isUnique)
                        repeatingLetterWord.add(word);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        System.out.println("Dictionary Size: " + dictionary.size());
    }

    private boolean isValidWord(String word) throws InvalidWordLengthException {
        if (word.length() != 5)
            throw new InvalidWordLengthException();
        return dictionary.contains(word);
    }

    String getRandomWord() {
        Object[] arr;
        if (repeatingLetterWord.size() == 0 || type == PlayerType.USER)
            arr = dictionary.toArray();
        else
            arr = repeatingLetterWord.toArray();
        return (String) arr[(int) (Math.random() * arr.length)];
    }

    private String getRandomSecretWord() {
        Object[] arr = dictionary.toArray();
        return (String) arr[(int) (Math.random() * arr.length)];
    }

    String getWord() {
        return word;
    }

    boolean useWord(String word) throws InvalidWordLengthException {
        if (!isValidWord(word))
            return false;
        dictionary.remove(word);
        repeatingLetterWord.remove(word);
        used.add(word);
        return true;
    }

    int getOpponentScore(String guess) {
        int score = 0;
        for (int i = 0; i < guess.length(); i++) {
            if (word.indexOf(guess.charAt(i)) >= 0)
                score++;
        }
        return score;
    }

    boolean isCorrectGuess(String guess) {
        return guess.equals(word);
    }

    void updateDictionary(String myGuess, int myScore) {
        if (type == PlayerType.USER)
            return;
        // Update History
        history.add(new GameRecord(myGuess, myScore));
        // Remove permutations
        ArrayList<String> temp = new ArrayList<>();
        for (String str : dictionary) {
            int score = 0;
            for (int i = 0; i < myGuess.length(); i++) {
                if (str.indexOf(myGuess.charAt(i)) >= 0)
                    score++;
            }
            if (score != 5 && myScore == 5) {
                temp.add(str);
            } else if (score == 5 && myScore < 5) {
                temp.add(str);
            } else if (score > 0 && myScore == 0) {
                temp.add(str);
            }
        }
        for (String str : temp) {
            dictionary.remove(str);
            repeatingLetterWord.remove(str);
        }
        if (myScore == 0) {
            System.out.printf("\t%d word left\n", dictionary.size());
            return;
        }
        // If a character occurrence > myScore, remove all words with that char
        ArrayList<Character> unwantedChars = new ArrayList<>();
        String tempStr = myGuess;
        while (tempStr.length() > myScore) {
            int occurrence = tempStr.length();
            char ch = tempStr.charAt(0);
            tempStr = tempStr.replaceAll("" + ch, "");
            occurrence -= tempStr.length();
            if (occurrence > myScore) {
                unwantedChars.add(ch);
            }
        }
        temp = new ArrayList<>();
        notContain.addAll(unwantedChars);
        for (String str : dictionary) {
            for (Character ch : unwantedChars) {
                if (str.indexOf(ch) >= 0) {
                    temp.add(str);
                    break;
                }
            }
        }
        for (String str : temp) {
            dictionary.remove(str);
            repeatingLetterWord.remove(str);
        }
        // If a character occurrence == myScore >= 3, remove all words without that char
        if (myScore >= 3) {
            Character mustContainChar = null;
            unwantedChars = new ArrayList<>();
            for (int i = 0; i < myGuess.length(); i++) {
                tempStr = myGuess.replaceAll("" + myGuess.charAt(i), "");
                if (myScore != myGuess.length() - tempStr.length())
                    break;
                if (tempStr.length() <= 2) {
                    mustContainChar = myGuess.charAt(i);
                    for (i = 0; i < tempStr.length(); i++) {
                        unwantedChars.add(tempStr.charAt(i));
                    }
                    break;
                }
            }
            if (mustContainChar != null) {
                mustContain.add(mustContainChar);
                notContain.addAll(unwantedChars);

                temp = new ArrayList<>();
                for (String str : dictionary) {
                    for (Character ch : unwantedChars) {
                        if (str.indexOf(ch) >= 0 || str.indexOf(mustContainChar) < 0) {
                            temp.add(str);
                            break;
                        }
                    }
                }
                for (String str : temp) {
                    dictionary.remove(str);
                    repeatingLetterWord.remove(str);
                }
            }
        }
        // Filter Based On History
        for (int i = 0; i < 5 && i < history.size(); i++)
            filterBasedOnHistory();
        // Print
        System.out.printf("\t%d word left\t\t%d\t\t[%d, %d]\n",
                dictionary.size(),
                repeatingLetterWord.size(),
                mustContain.size(),
                notContain.size());

    }

    private void filterBasedOnHistory() {
        ArrayList<Character> unwantedChars;
        ArrayList<Character> wantedChars;
        ArrayList<String> tempList;
        for (GameRecord record : history) {
            unwantedChars = new ArrayList<>();
            wantedChars = new ArrayList<>();
            tempList = new ArrayList<>();

            String temp = record.word;
            int score = record.score;
            int length = temp.length();
            for (Character ch : mustContain) {
                temp = temp.replaceAll("" + ch, "");
                score -= length - temp.length();
                length = temp.length();
            }
            for (Character ch : notContain) {
                temp = temp.replaceAll("" + ch, "");
            }
            length = temp.length();
            if (score == 0 && length > 0) {
                for (int i = 0; i < length; i++) {
                    unwantedChars.add(temp.charAt(i));
                    notContain.add(temp.charAt(i));
                }
            } else if (score > 0 && score == length) {
                for (int i = 0; i < length; i++) {
                    wantedChars.add(temp.charAt(i));
                    mustContain.add(temp.charAt(i));
                }
            }

            // Removal
            for (String str : dictionary) {
                boolean valid = true, hasMustContain = false;
                if (mustContain.size() == 0)
                    hasMustContain = true;
                for (int i = 0; i < str.length(); i++) {
                    char ch = str.charAt(i);
                    if (notContain.contains(ch)) {
                        valid = false;
                    }
                    if (mustContain.contains(ch)) {
                        hasMustContain = true;
                    }
                }
                if (!valid || !hasMustContain) {
                    tempList.add(str);
                }
            }
            for (String str : tempList) {
                dictionary.remove(str);
                repeatingLetterWord.remove(str);
            }
        }

        if (mustContain.size() == 5) {
            tempList = new ArrayList<>();
            char ch;
            for (String str : dictionary) {
                for (int i = 0; i < str.length(); i++) {
                    ch = str.charAt(i);
                    if ((str.indexOf(ch) != str.lastIndexOf(ch)) || !mustContain.contains(ch)) {
                        tempList.add(str);
                        break;
                    }
                }
            }
            for (String str : tempList) {
                dictionary.remove(str);
                repeatingLetterWord.remove(str);
            }
        }

    }

}